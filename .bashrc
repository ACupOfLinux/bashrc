#!/bin/bash
shopt -s autocd
shopt -s cdspell
export EDITOR='vi'
export VISUAL='vi'
HISTSIZE= HISTFILESIZE=
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
alias v='sudo nvim'
alias top='bpytop'
alias installpkg='sudo xbps-install -Suv'
alias updatesystem='sudo xbps-install -yu'
alias enterserver='ssh user@10.0.0.182'
alias installfromrepo='xbps-install --repository=$PWD'
alias makerepo='xbps-rindex -a'
alias bashrc='sudo nvim /home/kayden/.bashrc'
alias alacrittyconfig='sudo nvim /home/kayden/.config/alacritty/alacritty.toml'
alias ls='eza -al --color=always --group-directories-first' # my preferred listing
alias la='eza -a --color=always --group-directories-first'  # all files and dirs
alias ll='eza -l --color=always --group-directories-first'  # long format
alias lt='eza -aT --color=always --group-directories-first' # tree listing
alias l.='eza -a | egrep "^\."'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias gc='git clone'
alias checkupdates='xbps-install -un | wc -l'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias cat='bat'
colorscript -e suckless
alias vm='sudo virt-manager'
